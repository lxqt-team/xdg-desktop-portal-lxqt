Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xdg-desktop-portal-lxqt
Source: https://github.com/lxqt/xdg-desktop-portal-lxqt

Files: *
Copyright: 2021-2023 LXQt team
License: LGPL-2.1+

Files: src/main.cpp
       src/desktopportal.h
       src/desktopportal.cpp
       src/filechooser.cpp
       src/filechooser.h
Copyright: 2016 Red Hat Inc
           2016 Jan Grulich <jgrulich@redhat.com>
License: LGPL-2.0+

Files: src/utils.h
       src/utils.cpp
Copyright: 2018 Alexander Volkov <a.volkov@rusbitech.ru>
License: LGPL-2.0+

Files: debian/*
Copyright: 2022-2023 Palo Kisa <palo.kisa@gmail.com>
           2023-2024 Andrew Lee (李健秋) <ajqlee@debian.org>
           2023      ChangZhuo Chen (陳昌倬) <czchen@debian.org>
           2024      Severus Septimius <severusseptimius7@gmail.com>
License: GPL-2+

License: LGPL-2.1+
 This program or library is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: GPL-2+
 This program or library is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.0+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".
